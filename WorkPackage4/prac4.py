#!/usr/bin/python3

import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import threading
import time
import RPi.GPIO as GPIO

# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
# create the chip select
cs = digitalio.DigitalInOut(board.D5)
# create the mcp object
mcp = MCP.MCP3008(spi, cs)
# crete an analog input channel on pin 2
chan2 = AnalogIn(mcp, MCP.P2)
# create an analog input channel on pin 1
chan1 = AnalogIn(mcp, MCP.P1)

toggle_btn = 24
start_time = time.time()
rate = 10
i = 0

# setup GPIO pin for the toggle button
def setup():
  GPIO.setwarnings(False) # Ignore warning for now
  # setup GPIO mode
  GPIO.setmode(GPIO.BCM)
  # setup GPIO input for the toggle button
  GPIO.setup(toggle_btn, GPIO.IN, pull_up_down = GPIO.PUD_UP)
  # setup interrupt for button and debounce button
  GPIO.add_event_detect(toggle_btn, GPIO.FALLING, callback = call_back, bouncetime = 500)

# callback function
def call_back(channel):
  sampling_rate()

# press button to set the sampling rate
def sampling_rate():
  global rate, i
  if (i==0):
    rate = 5

  elif (i==1):
    rate = 1
  i+=1

# get the temperature readings of the sensor
def temperature():
  return chan1.value

# convert temperature sensor output to degrees Celsius
def convert_temp():
  temp_volt = chan1.voltage
  # Temp in degree C = (SensorOutputVoltage-SensorOutputVoltage at 0 degrees C)/temperatureCoefficient
  temp_C = round((temp_volt - 0.5)/0.01, 2)
  return str(temp_C)+' C'

# get the ADC value of the LDR
def ldr():
  return chan2.value


# calculate the runtime of the thread
def run_time():
  current_time = time.time()
  i = 0
  while (i == 0):
    runtime = int(current_time - start_time)
    return str(runtime)+'s'
    time.sleep(rate)

def print_reading_thread():
  thread = threading.Timer(rate, print_reading_thread)
  thread.daemon = True
  thread.start()
  print("{:<10} {:<15} {:<15} {:<15}".format(run_time(), temperature(), convert_temp(), ldr()))


if __name__ == "__main__":
  setup()
  print("{:<10} {:<15} {:<15} {:<15}".format('Runtime', 'Temp Reading', 'Temp', 'Light Reading'))
  print_reading_thread()
  while True:
    pass
 
  GPIO.cleanup()
