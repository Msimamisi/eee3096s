# Import libraries
import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os
import time

#some global variables that need to change as we run the program
end_of_game = None  # set if the user wins or ends the game
current_guess = 0

# DEFINE THE PINS USED HERE
LED_value = [11, 13, 15]
LED_accuracy = 32
btn_submit = 16
btn_increase = 18
buzzer = None
eeprom = ES2EEPROMUtils.ES2EEPROM()


# Print the game banner
def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print("| \ | |               | |                / ____| |          / _|/ _| |")
    print("|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print("| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print("| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print("|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")


# Print the game menu
def menu():
    global end_of_game
    option = input("Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()
    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)
    elif option == "P":
        os.system('clear')
        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")
        value = generate_number()
        while not end_of_game:
            pass
    elif option == "Q":
        print("Come back soon!")
        exit()
    else:
        print("Invalid option. Please select a valid one!")


def display_scores(count, raw_data):
    # print the scores to the screen in the expected format
    print("There are {} scores. Here are the top 3!".format(count))
    # print out the scores in the required format
    pass


# Setup Pins
def setup():
    # Setup board mode
    GPIO.setmode(GPIO.BOARD)

    # Setup regular GPIO
    GPIO.setup(btn_increase, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(btn_submit, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(LED_value[0], GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(LED_value[1], GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(LED_value[2], GPIO.OUT, initial=GPIO.LOW)
    red_LED_PWM = GPIO.setup(LED_accuracy, GPIO.OUT)
    buzzer_PWM = GPIO.setup(33, GPIO.OUT)

    # Setup PWM channels
    red_LED_PWM =  GPIO.PWM(LED_accuracy,50)
    buzzer_PWM = GPIO.PWM(33,50)

    red_LED_PWM.start(0)
    buzzer_PWM.start(0)


    # Setup debouncing and callbacks(calls back a function on every event trigger--> interrupt)
    GPIO.add_event_detect(btn_increase, GPIO.FALLING, callback=btn_increase_pressed, bouncetime=500)
    GPIO.add_event_detect(btn_submit, GPIO.FALLING, callback=btn_guess_pressed, bouncetime=500)
    


# Load high scores
def fetch_scores():
    scores = []
    # get however many scores there are
    score_count = eeprom.read_block(0,1)
    score_count = score_count[0]

    # Get the scores
    blocks = eeprom.read_block(1,score_count)

    #convert the score back to ascii
    name=""
    score=0
    name_score=[]
    count=0

    for column in blocks:
        if count == 3:
            score = column
            name_score.append(name)
            name_score.append(score)
            scores.append(name_score)
        else:
            name+=chr(column)
            count+=1

    # return back the results
    return score_count, scores


# Save high scores
def save_scores():
    # fetch scores
    global current_guess
    score_count, scores = fetch_scores()

    # include new score
    name = input("Enter your name")
    scores.append(name[0:3],current_guess)

    # sort
    scores.sort(key=lambda x: x[1])

    # update total amount of scores
    score_count+=1

    # write new scores
    eeprom.write_blocks(0, score_count)
    eeprom.write_blocks(1, scores)
    pass


# Generate guess number
def generate_number():
    return random.randint(0, pow(2, 3)-1)

secret_number = generate_number()
# Increase button pressed
def btn_increase_pressed(channel):
    # Increase the value shown on the LEDs
    # You can choose to have a global variable store the user's current guess, 
    # or just pull the value off the LEDs when a user makes a guess
    global current_guess
    
    if current_guess < 7:
        current_guess+=1
    else:
        current_guess-=7

    guess = bin(current_guess)[2:]

    if len(guess)<3:
        zeros = 3-len(guess)
        guess = zeros*"0"+guess

    print(guess)
    GPIO.output(LED_value[0], int(guess[0]))
    GPIO.output(LED_value[1], int(guess[1]))
    GPIO.output(LED_value[2], int(guess[2]))
    return current_guess


# Guess button
def btn_guess_pressed(channel):
    global current_guess
    global secret_number
    # If they've pressed and held the button, clear up the GPIO and take them back to the menu screen
    time.sleep(2)
    if GPIO.input(btn_submit) != 1:
        GPIO.cleanup()
        os.system("python3 p3.py")
    

    # Compare the actual value with the user value displayed on the LEDs
    # Change the PWM LED
    # if it's close enough, adjust the buzzer
    # if it's an exact guess:
    # - Disable LEDs and Buzzer
    # - tell the user and prompt them for a name
    # - fetch all the scores
    # - add the new score
    # - sort the scores
    # - Store the scores back to the EEPROM, being sure to update the score count
    if current_guess == secret_number:
       GPIO.cleanup()
       print("You have guessed correctly")
       name = input("Please enter your name\n")
       score_count = eeprom.read_block(0,1)
       scores = eeprom.read_block(1,score_count)
       scores.append(btn_increase_pressed())
       score_count+=1
       scores.sort(lambda x: x[1])
       eeprom.write_block(0,score_count)
       eeprom.write_block(1, scores)
    else:
        accuracy_leds()
        trigger_buzzer()
    


# LED Brightness
def accuracy_leds():
    global current_guess
    global secret_number
    # Set the brightness of the LED based on how close the guess is to the answer
    # - The % brightness should be directly proportional to the % "closeness"
    if secret_number == 0:
        denom_secret_number = 1
    else:
        denom_secret_number = secret_number

    compare_values = abs((secret_number-current_guess)/denom_secret_number)*100

    GPIO.PWM(32,compare_values)
    # - For example if the answer is 6 and a user guesses 4, the brightness should be at 4/6*100 = 66%
    # - If they guessed 7, the brightness would be at ((8-7)/(8-6)*100 = 50%


# Sound Buzzer
def trigger_buzzer():
    global current_guess
    global secret_number
    # The buzzer operates differently from the LED
    # While we want the brightness of the LED to change(duty cycle), we want the frequency of the buzzer to change
    # The buzzer duty cycle should be left at 50%
    # If the user is off by an absolute value of 3, the buzzer should sound once every second
    # If the user is off by an absolute value of 2, the buzzer should sound twice every second
    # If the user is off by an absolute value of 1, the buzzer should sound 4 times a second
    if abs(current_guess-secret_number) == 3:
        buzzer.ChangeFrequency(1)
    elif abs(current_guess-secret_number) == 2:
        buzzer.ChangeFrequency(2)
    elif abs(current_guess-secret_number) == 1:
        buzzer.ChangeFrequency(4)


if __name__ == "__main__":
    try:
        # Call setup function
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
